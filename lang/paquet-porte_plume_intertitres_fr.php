<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

return [
	// P
	'porte_plume_intertitres_description' => 'Ajoute aux raccourcis de spip, le niveaux de titre',
	'porte_plume_intertitres_nom'         => 'Intertitres hierarchiques',
	'porte_plume_intertitres_slogan'      => 'Intertitres hierarchiques',
];
