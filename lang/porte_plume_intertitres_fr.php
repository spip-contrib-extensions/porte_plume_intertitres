<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

return [
	// B
	'barre_intertitre2' => '{{{** niveau 2 }}}',
	'barre_intertitre3' => '{{{*** niveau 3 }}}',
	'barre_intertitre4' => '{{{**** niveau 4 }}}',
	'barre_intertitre5' => '{{{***** niveau 5 }}}',

	// C
	'cfg_titre_configurer'          => 'Configurer les intertitres',
	'cfg_references_fieldset'       => 'Paramètres des titres numérotés',
	'cfg_references_explication'    => 'Les titres numérotés permettent d\'organiser un contenu de manière plus hiérarchique, en plus de la sémantique.',
	'cfg_label_afficher_references' => 'Afficher dans le porte-plume les boutons d\'ajout des titres numerotés',

	// P
	'porte_plume_intertitres_titre' => 'Intertitres hiérarchiques',
	'porte_plume_intertitres'       => 'Intertitres',
];
